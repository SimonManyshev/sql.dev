--- Создание таблицы Пользователи. User - зарезервированное слово в Postgres, поэтому Users
DROP TABLE IF EXISTS Users CASCADE;
CREATE TABLE Users (
   id INT PRIMARY KEY NOT NULL,
   name VARCHAR(255) NOT NULL
);
CREATE INDEX idx_user_id ON Users(id);

COMMENT ON TABLE Users IS 'Пользователи';
COMMENT ON COLUMN Users.id IS 'Уникальный ID пользователя';
COMMENT ON COLUMN Users.name IS 'Имя пользователя';

INSERT INTO Users (id, name) VALUES (1, 'Вова');
INSERT INTO Users (id, name) VALUES (2, 'Петя');
INSERT INTO Users (id, name) VALUES (3, 'Маша');
INSERT INTO Users (id, name) VALUES (4, 'Даша');
INSERT INTO Users (id, name) VALUES (5, 'Вова2');
INSERT INTO Users (id, name) VALUES (6, 'Ашот');


--- Создание таблицы Продукты
DROP TABLE IF EXISTS Product CASCADE;
CREATE TABLE Product (
     id INT PRIMARY KEY NOT NULL,
     name VARCHAR(255) NOT NULL
);
CREATE INDEX idx_product_id ON Product(id);

COMMENT ON TABLE Product IS 'Продукты';
COMMENT ON COLUMN Product.id IS 'Уникальный ID Продукта';
COMMENT ON COLUMN Product.name IS 'Наименование продукта';

INSERT INTO Product (id, name) VALUES (1, 'Молоток');
INSERT INTO Product (id, name) VALUES (2, 'Топор');
INSERT INTO Product (id, name) VALUES (3, 'Чайник');
INSERT INTO Product (id, name) VALUES (4, 'Айфон');
INSERT INTO Product (id, name) VALUES (5, 'Вантуз');
INSERT INTO Product (id, name) VALUES (6, 'Пакет');


--- Создание таблицы Корзина
DROP TABLE IF EXISTS Cart CASCADE;
CREATE TABLE Cart (
    id INT PRIMARY KEY NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX idx_cart_id ON Cart(id);

COMMENT ON TABLE Cart IS 'Корзина';
COMMENT ON COLUMN Cart.id IS 'Уникальный ID Корзины';
COMMENT ON COLUMN Cart.created_at IS 'Время создания корзины';

INSERT INTO Cart (id) VALUES (1);
INSERT INTO Cart (id) VALUES (2);
INSERT INTO Cart (id) VALUES (3);
INSERT INTO Cart (id) VALUES (4);
INSERT INTO Cart (id) VALUES (5);
INSERT INTO Cart (id) VALUES (6);


--- Создание таблицы Позиция
DROP TABLE IF EXISTS Position CASCADE;
CREATE TABLE Position (
    id INT PRIMARY KEY NOT NULL,
    user_id INT NOT NULL,
    product_id INT NOT NULL,
    cart_id INT NOT NULL,
    quantity INT NOT NULL DEFAULT 1,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (product_id) REFERENCES Product(id),
    FOREIGN KEY (cart_id) REFERENCES Cart(id)
);
CREATE INDEX idx_position_user_id ON Position(user_id);
CREATE INDEX idx_position_product_id ON Position(product_id);
CREATE INDEX idx_position_user_product ON Position(user_id, product_id);

COMMENT ON TABLE Position IS 'Позиция в корзине покупателя';
COMMENT ON COLUMN Position.id IS 'Уникальный ID Позиции';
COMMENT ON COLUMN Position.user_id IS 'Ссылка на Пользователя';
COMMENT ON COLUMN Position.product_id IS 'Ссылка на Продукт';
COMMENT ON COLUMN Position.product_id IS 'Ссылка на Корзину';
COMMENT ON COLUMN Position.quantity IS 'Количество штук продуктов в корзине';

INSERT INTO Position (id, user_id, product_id, cart_id, quantity) VALUES (1, 1, 1, 1, 2);
INSERT INTO Position (id, user_id, product_id, cart_id, quantity) VALUES (2, 1, 2, 1, 8);

INSERT INTO Position (id, user_id, product_id, cart_id, quantity) VALUES (3, 2, 3, 2, 12);
INSERT INTO Position (id, user_id, product_id, cart_id, quantity) VALUES (4, 2, 4, 2, 23);

INSERT INTO Position (id, user_id, product_id, cart_id, quantity) VALUES (5, 3, 5, 3, 12);
INSERT INTO Position (id, user_id, product_id, cart_id, quantity) VALUES (6, 3, 6, 2, 23);