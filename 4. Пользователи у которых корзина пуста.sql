--- 4. Написать запрос, который получает список всех пользователей, у которых корзина пуста
SELECT Users.name
FROM Users
LEFT JOIN Position ON Users.id = Position.user_id
GROUP BY Users.name
HAVING COUNT(Position.id) = 0;